# terraform config
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

# aws config
provider "aws" {
  region = "us-east-1"
}

# S3 Bucket for hosting the website
resource "aws_s3_bucket" "hugo_site" {
  bucket = "datadog-hugo-site-bucket"
}

resource "aws_s3_bucket_public_access_block" "bucket_access_block" {
  bucket = aws_s3_bucket.hugo_site.id

  block_public_acls   = false
  block_public_policy = false
}

# Bucket policy to allow public read access
resource "aws_s3_bucket_policy" "hugo_site_policy" {
  bucket = aws_s3_bucket.hugo_site.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action    = ["s3:GetObject"],
        Effect    = "Allow",
        Resource  = ["arn:aws:s3:::${aws_s3_bucket.hugo_site.bucket}/*"],
        Principal = "*"
      }
    ]
  })
}

# CORS Configuration
resource "aws_s3_bucket_cors_configuration" "hugo_site_cors" {
  bucket = aws_s3_bucket.hugo_site.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }
}

# Website Configuration
resource "aws_s3_bucket_website_configuration" "hugo_site_website" {
  bucket = aws_s3_bucket.hugo_site.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }
}




