import * as http from "http";
import * as fs from "fs";
import * as path from "path";

// Directory to save downloaded images
const imagesDir = path.join(__dirname, "static", "img", "products");
if (!fs.existsSync(imagesDir)) {
  fs.mkdirSync(imagesDir, { recursive: true });
}

// Function to download image
const downloadImage = (url: string, filename: string) => {
  http
    .get(url, (res) => {
      const filePath = path.join(imagesDir, filename);
      const fileStream = fs.createWriteStream(filePath);
      res.pipe(fileStream);
      fileStream.on("finish", () => {
        fileStream.close();
        console.log(`Downloaded ${filename}`);
      });
    })
    .on("error", (err) => {
      console.error(`Error downloading ${filename}: ${err.message}`);
    });
};

// Fetch the list of images
http
  .get("http://localhost:3000", (res) => {
    let data = "";
    res.on("data", (chunk) => {
      data += chunk;
    });
    res.on("end", () => {
      const products: { url: string; name: string }[] =
        JSON.parse(data).products;
      products.forEach((product) => {
        downloadImage(product.url, product.name);
      });
    });
  })
  .on("error", (err) => {
    console.error(`Error fetching product list: ${err.message}`);
  });
